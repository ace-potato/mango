import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from './auth/auth.module';
import { MaterialModule } from '../core/material.module';
import { MapsModule } from './maps/maps.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { InboxModule } from './inbox/inbox.module';
import { GanttModule } from './gantt/gantt.module';
import { CalendarModule } from './calendar/calendar.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AuthModule,
    LeafletModule.forRoot()
  ],
  declarations: []
})
export class PagesModule { }
