import { RouterModule, Routes } from '@angular/router';
import { GanttComponent } from './gantt.component';
import { NgModule } from '@angular/core';

const ganttRoutes: Routes = [
  { path: '', component: GanttComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(ganttRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class GanttRoutingModule { }


