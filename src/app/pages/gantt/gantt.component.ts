import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'vr-gantt',
  templateUrl: './gantt.component.html',
  styleUrls: ['./gantt.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class GanttComponent implements OnInit {


  constructor(@Inject(PLATFORM_ID) private platformId: any) { }

  ngOnInit() {
  }



  isPlatformBrowser() {
    return isPlatformBrowser(this.platformId);
  }
}
