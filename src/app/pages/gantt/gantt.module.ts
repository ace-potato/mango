import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GanttComponent } from './gantt.component';
import { GanttRoutingModule } from 'app/pages/gantt/gantt.routing';

@NgModule({
  imports: [
    CommonModule,
    GanttRoutingModule
  ],
  declarations: [GanttComponent]
})
export class GanttModule { }
