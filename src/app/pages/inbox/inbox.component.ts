import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';
import { isPlatformBrowser } from '@angular/common';
import { messages } from './inbox.data';
import { MatSnackBar, MatDialog } from '@angular/material';
import { NewMessageComponent } from 'app/pages/inbox/new-message/new-message.component';
@Component({
  selector: 'vr-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class InboxComponent implements OnInit {
  messages = messages;
  ngOnInit(): void {
  }

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog) { }

  onRemove(index: number): void {
    const copy = [...this.messages];
    copy.splice(index, 1);
    this.messages = copy;
  }

  onNewMessage(data: any = {}): void {
    const dialogRef = this.dialog.open(NewMessageComponent, {
      width: '75%',
      panelClass: 'new-message-dialog',
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('Email sent!', null, {
          duration: 2000
        });
      }
    });
  }
}
