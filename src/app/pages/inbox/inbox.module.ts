import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox.component';
import { InboxRoutingModule } from 'app/pages/inbox/inbox.routing';
import { MaterialModule } from 'app/core/material.module';
import { MessageComponent } from 'app/pages/inbox/message/message.component';
import { NewMessageComponent } from 'app/pages/inbox/new-message/new-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MessageComponent,
    NewMessageComponent,
    InboxComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    InboxRoutingModule
  ],
  entryComponents: [
    NewMessageComponent
  ]
})
export class InboxModule { }
