import { Component, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { Observable } from 'rxjs/Observable';

const COMMA = 188;

@Component({
  selector: 'vr-inbox-new-message',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.scss']
})
export class NewMessageComponent {

  separatorKeysCodes = [ENTER, COMMA];

  contacts: string[] = [
    'Austin Mcdaniel',
    'Jeremy Elbourn',
    'Jules Kremer',
    'Brad Green',
    'Tina Gao'
  ];
  recipients: string[] = [];
  subjectCtrl = new FormControl();
  bodyCtrl = new FormControl();
  recipientsCtrl = new FormControl();
  filteredContacts: Observable<any[]>;

  @ViewChild('recipientInput') recipientInput;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.to && data.subject) {
      this.recipients.push(data.to);
      this.subjectCtrl.setValue(data.subject);
    }

    this.filteredContacts = this.recipientsCtrl.valueChanges
      .startWith(null)
      .map(contact => contact ? this.filterContacts(contact) : this.contacts.slice());
  }

  addRecipient(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our person
    if ((value || '').trim()) {
      this.recipients.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeRecipient(recipient: string): void {
    const index = this.recipients.indexOf(recipient);
    if (index >= 0) {
      this.recipients.splice(index, 1);
    }
  }

  filterContacts(name: string): string[] {
    return this.contacts.filter(contact =>
      contact.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent): void {
    this.recipients.push(event.option.value);
    this.recipientInput.nativeElement.value = '';
  }

}
