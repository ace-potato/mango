import { RouterModule, Routes } from '@angular/router';
import { InboxComponent } from './inbox.component';
import { NgModule } from '@angular/core';

const inboxRoutes: Routes = [
  { path: '', component: InboxComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(inboxRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class InboxRoutingModule { }


