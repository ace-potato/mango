import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { RouterModule, Routes } from '@angular/router';
import { FuseScrumboardComponent } from './scrumboard.component';
import { BoardResolve, ScrumboardService } from './scrumboard.service';
import { FuseScrumboardBoardComponent, NewProjectDialogComponent } from './board/board.component';
import { FuseScrumboardBoardListComponent } from './board/list/list.component';
import { FuseScrumboardBoardCardComponent } from './board/list/card/card.component';
import { FuseScrumboardBoardEditListNameComponent } from './board/list/edit-list-name/edit-list-name.component';
import { FuseScrumboardBoardAddCardComponent } from './board/list/add-card/add-card.component';
import { FuseScrumboardBoardAddListComponent } from './board/add-list/add-list.component';
import { FuseScrumboardCardDialogComponent } from './board/dialogs/card/card.component';
import { FuseScrumboardLabelSelectorComponent } from './board/dialogs/card/label-selector/label-selector.component';
import { FuseScrumboardEditBoardNameComponent } from './board/edit-board-name/edit-board-name.component';
import { FuseScrumboardBoardSettingsSidenavComponent } from './board/sidenavs/settings/settings.component';
import { FuseScrumboardBoardColorSelectorComponent } from './board/sidenavs/settings/board-color-selector/board-color-selector.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FusePipesModule } from 'app/core/pipes/pipes.module';
import { MatSidenavModule, MatMenuModule, MatTooltipModule, MatFormFieldModule, MatCheckboxModule, MatProgressBarModule, MatChipsModule, MatDatepickerModule, MatToolbarModule, MatDividerModule, MatListModule, MatInputModule, MatSelectChange, MatSelectModule, MatTabsModule } from '@angular/material';
import { FuseMaterialColorPickerComponent } from 'app/core/material-color-picker/material-color-picker.component';
import { ScrumboardProjectComponent } from './project/project.component';
import { ScrumboardRoutingModule } from './scrumboard.routing';
import { NgcFloatButtonModule } from 'ngc-float-button';
import { ProjectsDashboardService } from './project/projects.service';
import { MaterialModule } from '../../core/material.module';


@NgModule({
    declarations: [
        FuseScrumboardComponent,
        FuseMaterialColorPickerComponent,
        FuseScrumboardBoardComponent,
        FuseScrumboardBoardListComponent,
        FuseScrumboardBoardCardComponent,
        FuseScrumboardBoardEditListNameComponent,
        FuseScrumboardBoardAddCardComponent,
        FuseScrumboardBoardAddListComponent,
        FuseScrumboardCardDialogComponent,
        NewProjectDialogComponent,
        FuseScrumboardLabelSelectorComponent,
        FuseScrumboardEditBoardNameComponent,
        FuseScrumboardBoardSettingsSidenavComponent,
        FuseScrumboardBoardColorSelectorComponent
    ],
    imports: [
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FusePipesModule,
        MaterialModule,
        NgxDnDModule,
        NgcFloatButtonModule,
        ScrumboardRoutingModule
    ],
    providers: [
        ScrumboardService,
        BoardResolve,
        ProjectsDashboardService
    ],
    entryComponents: [FuseScrumboardCardDialogComponent, NewProjectDialogComponent]
})
export class FuseScrumboardModule {
}
