import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FuseScrumboardBoardComponent } from './board/board.component';
import { BoardResolve } from 'app/pages/scrumboard/scrumboard.service';
import { ScrumboardProjectComponent } from 'app/pages/scrumboard/project/project.component';

const boardRoutes: Routes = [
    {
        path: '',
        component: FuseScrumboardBoardComponent,
        resolve: {
            board: BoardResolve
        },
        children: [
            {
                path: 'project',
                loadChildren: 'app/pages/scrumboard/project/project.module#ScrumboardProjectModule',
                pathMatch: 'prefix'

            }
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(boardRoutes)
    ],
    providers: [
        BoardResolve
    ],
    exports: [
        RouterModule
    ]
})
export class ScrumboardRoutingModule { }


