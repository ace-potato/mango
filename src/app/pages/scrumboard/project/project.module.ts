import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScrumboardProjectComponent } from './project.component';
import { ProjectsDashboardService } from './projects.service';
import { MaterialModule } from '../../../core/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        component: ScrumboardProjectComponent,
        resolve: {
            data: ProjectsDashboardService
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        ScrumboardProjectComponent
    ],
    providers: [
        ProjectsDashboardService
    ]
})
export class ScrumboardProjectModule {
}

