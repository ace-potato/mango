import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ProjectsDashboardService } from './projects.service';
import * as shape from 'd3-shape';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { fuseAnimations } from 'app/core/animations';

@Component({
    selector: 'vr-scrumboard-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ScrumboardProjectComponent implements OnInit, OnDestroy {
    projects: any[];
    selectedProject: any;



    dateNow = Date.now();

    constructor(private projectsDashboardService: ProjectsDashboardService) {
        this.projects = this.projectsDashboardService.projects;



    }

    ngOnInit() {

    }

    ngOnDestroy() {
    }

}



