import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScrumboardService } from '../scrumboard.service';
import { Subscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';
import { List } from '../list.model';
import { fuseAnimations } from 'app/core/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'vr-scrumboard-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss'],
    animations: fuseAnimations
})
export class FuseScrumboardBoardComponent implements OnInit, OnDestroy {
    board: any;
    onBoardChanged: Subscription;
    direction: String = 'bottom';

    constructor(
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private location: Location,
        private scrumboardService: ScrumboardService
    ) {
    }

    ngOnInit() {
        this.onBoardChanged =
            this.scrumboardService.onBoardChanged
                .subscribe(board => {
                    this.board = board;
                });
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(NewProjectDialogComponent, {

            width: '85%',
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('Save off project JSON');
        });
    }

    onListAdd(newListName) {
        if (newListName === '') {
            return;
        }

        this.scrumboardService.addList(new List({ name: newListName }));
    }

    onBoardNameChanged(newName) {
        this.scrumboardService.updateBoard();
        this.location.go('/apps/scrumboard/boards/' + this.board.id + '/' + this.board.uri);
    }

    onDrop(ev) {
        this.scrumboardService.updateBoard();
    }

    ngOnDestroy() {
        this.onBoardChanged.unsubscribe();
    }
}

@Component({
    selector: 'vr-new-project-dialog',
    styleUrls: ['./dialogs/project-details/project.details.component.scss'],
    templateUrl: './dialogs/project-details/project.details.component.html',
})
export class NewProjectDialogComponent implements OnInit {
    options: FormGroup;
    isFormReady: Boolean = false;
    statuses = [
        { value: 'discovery', viewValue: 'Discovery' },
        { value: 'planning', viewValue: 'Planning' },
        { value: 'execution', viewValue: 'Execution' },
        { value: 'now', viewValue: 'In Progress' },
        { value: 'completed', viewValue: 'Completed' },
        { value: 'cancelled', viewValue: 'Cancelled' }
    ];
    constructor(
        public dialogRef: MatDialogRef<NewProjectDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any, fb: FormBuilder) {
        this.options = fb.group({
            hideRequired: false,
            floatLabel: 'auto',
        });
    }

    ngOnInit() {
        console.log('ngOnInit');
        this.isFormReady = true;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
