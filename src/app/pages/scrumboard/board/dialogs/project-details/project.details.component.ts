import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MaterialModule } from 'app/core/material.module';


/**
 * @title Stepper overview
 */
@Component({
    selector: 'vr-overview-example',
    templateUrl: 'project.details.component.html',
    styleUrls: ['project.details.component.scss']
})
export class NewProjectComponent implements OnInit {
    options: FormGroup;
    isFormReady: Boolean = false;


    constructor(fb: FormBuilder) {
        this.options = fb.group({
            hideRequired: false,
            floatLabel: 'auto',
        });
        this.isFormReady = true;
        console.log('isFormReady: ' + this.isFormReady);
    }

    ngOnInit() {
        console.log('inside init');

    }
}
