import { Component, Inject, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'vr-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class CalendarComponent implements OnInit {

  // tslint:disable-next-line:prefer-const
  dateObj = new Date();
  // tslint:disable-next-line:prefer-const
  yearMonth = this.dateObj.getUTCFullYear() + '-' + (this.dateObj.getUTCMonth() + 1);
  data: any = [{
    title: 'All Day Event',
    start: this.yearMonth + '-01'
  },
  {
    title: 'Long Event',
    start: this.yearMonth + '-07',
    end: this.yearMonth + '-10'
  },
  {
    id: 999,
    title: 'Repeating Event',
    start: this.yearMonth + '-09T16:00:00'
  },
  {
    id: 999,
    title: 'Repeating Event',
    start: this.yearMonth + '-16T16:00:00'
  },
  {
    title: 'Conference',
    start: this.yearMonth + '-11',
    end: this.yearMonth + '-13'
  },
  {
    title: 'Meeting',
    start: this.yearMonth + '-12T10:30:00',
    end: this.yearMonth + '-12T12:30:00'
  },
  {
    title: 'Lunch',
    start: this.yearMonth + '-12T12:00:00'
  },
  {
    title: 'Meeting',
    start: this.yearMonth + '-12T14:30:00'
  },
  {
    title: 'Happy Hour',
    start: this.yearMonth + '-12T17:30:00'
  },
  {
    title: 'Dinner',
    start: this.yearMonth + '-12T20:00:00'
  },
  {
    title: 'Birthday Party',
    start: this.yearMonth + '-13T07:00:00'
  },
  {
    title: 'Click for Google',
    url: 'http://google.com/',
    start: this.yearMonth + '-28'
  }];


  constructor(@Inject(PLATFORM_ID) private platformId: any) { }

  ngOnInit() {

  }



  isPlatformBrowser() {
    return isPlatformBrowser(this.platformId);
  }
}
