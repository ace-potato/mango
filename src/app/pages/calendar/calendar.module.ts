import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar.component';
import { FullCalendarModule } from 'ng-fullcalendar';

import { CalendarRoutingModule } from 'app/pages/calendar/calendar.routing';

@NgModule({
  imports: [
    CommonModule,
    FullCalendarModule,
    CalendarRoutingModule
  ],
  declarations: [CalendarComponent]
})
export class CalendarModule { }
