import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar.component';
import { NgModule } from '@angular/core';

const calendarRoutes: Routes = [
  { path: '', component: CalendarComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(calendarRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CalendarRoutingModule { }


