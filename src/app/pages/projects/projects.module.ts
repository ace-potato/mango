import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { PageHeaderModule } from '../../core/page-header/page-header.module';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';
import { RouterModule } from '@angular/router';
import { ProjectsRoutingModule } from './projects.routing';
import { MaterialModule } from '../../core/material.module';

@NgModule({
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    FormsModule,
    RouterModule,
    PageHeaderModule,
    BreadcrumbsModule,
    FlexLayoutModule,
    MaterialModule
  ],
  declarations: [ProjectsComponent]
})
export class ProjectsModule { }
