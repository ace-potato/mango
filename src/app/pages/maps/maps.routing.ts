import { RouterModule, Routes } from '@angular/router';
import { MapsComponent } from './maps.component';
import { NgModule } from '@angular/core';

const mapRoutes: Routes = [
  { path: '', component: MapsComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(mapRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MapsRoutingModule { }


