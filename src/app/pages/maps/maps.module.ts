import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsComponent } from './maps.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapsRoutingModule } from 'app/pages/maps/maps.routing';

@NgModule({
  imports: [
    CommonModule,
    LeafletModule,
    MapsRoutingModule
  ],
  declarations: [MapsComponent]
})
export class MapsModule { }
