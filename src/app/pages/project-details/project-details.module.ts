import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDetailsComponent } from './project-details.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PageHeaderModule } from '../../core/page-header/page-header.module';
import { BreadcrumbsModule } from '../../core/breadcrumbs/breadcrumbs.module';

import { ProjectDetailsRoutingModule } from './project-details.routing';
import { MaterialModule } from '../../core/material.module';

@NgModule({
  imports: [
    CommonModule,
    ProjectDetailsRoutingModule,
    FlexLayoutModule,
    PageHeaderModule,
    BreadcrumbsModule,
    MaterialModule
  ],
  declarations: [ProjectDetailsComponent]
})
export class ProjectDetailsModule { }
