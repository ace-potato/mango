import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers/index';
import * as settings from '../settings/shared/settings.action';
import { MatRadioChange, MatSelectChange } from '@angular/material';
import 'rxjs/add/operator/takeUntil';
import { componentDestroyed } from '../utils/component-destroyed';
import { Filter, Account } from './shared/settings.model';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { SettingsService } from './shared/settings.service';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material/core';

@Component({
  selector: 'vr-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [
    SettingsService,
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class SettingsComponent implements OnInit, OnDestroy {
  filter: Filter;
  accounts: Account[];
  endDate: FormControl;
  startDate: FormControl;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.filter = new Filter();
    this.store
      .select(fromRoot.getFilters)
      .takeUntil(componentDestroyed(this))
      .subscribe(filter => {
        this.filter = filter;
      });
    this.startDate = new FormControl(moment([2017, 0, 1]));
    this.endDate = new FormControl(moment([2017, 11, 1]));
    this.generateAccounts();
  }

  generateAccounts(): void {
    this.accounts = [];
    const account = new Account();
      account.label = 'Account 114';
      account.value = '114';
      this.accounts.push(account);
      const account2 = new Account();
      account2.label = 'Account 126';
      account2.value = '126';
      this.accounts.push(account2);
      const account3 = new Account();
      account3.label = 'Account 109';
      account3.value = '109';
      this.accounts.push(account3);
      const account4 = new Account();
      account4.label = 'Account 134';
      account4.value = '134';
      this.accounts.push(account4);
      const account5 = new Account();
      account5.label = 'Account 127';
      account5.value = '127';
      this.accounts.push(account5);
  }

  setAccountChange(radioEvent: MatRadioChange) {
    this.filter.account = radioEvent.value;
  }

  applyFilters() {

    this.filter.endDate = this.endDate.value;
    this.filter.startDate = this.startDate.value;
    this.store.dispatch(new settings.ApplyFiltersAction(this.filter));
  }

  ngOnDestroy() { }
}
