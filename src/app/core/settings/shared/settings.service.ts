import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Filter, LogEntry, FilteredData } from './settings.model';

@Injectable()
export class SettingsService {
  constructor(private http: Http) { }

  getAccountData(filter: Filter): Observable<any> {
    const queryString = 'apikey=' + filter.account + '&startdate=' + filter.startDate + '&enddate=' + filter.endDate;
    console.log('queryString: ' + queryString);
    return this.http
      .get('http://localhost:3000/accountData?' + queryString)
      .map(res => res.json());
  }
}
