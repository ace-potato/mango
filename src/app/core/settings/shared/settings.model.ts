import * as moment from 'moment';
import { Moment } from 'moment';
export class Filter {
  account: string;
  startDate: Moment;
  endDate: Moment;
  apikey: string;
  success: boolean;
  statusCode: number;
  constructor() {
    this.account = '';
    this.startDate = moment();
    this.endDate = moment();
    this.apikey = '';
    this.success = false;
    this.statusCode = 200;
  }
}

export class FilteredData {
  data: LogEntry[];
}

export class LogEntry {
  timestamp: number;
  apikey: string;
  url: string;
  endpoint: string;
  userAgent: string;
  ip: string;
  success: boolean;
  errorCode: number;
  country: string;
  region: string;
  city: string;
}

export class Account {
  label: string;
  value: string;
}
