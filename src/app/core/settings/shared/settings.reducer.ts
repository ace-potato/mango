import * as settings from './settings.action';
import { updateObject } from '../../utils/update-object';
import { Filter, LogEntry } from 'app/core/settings/shared/settings.model';
import { FilteredData } from './settings.model';

export interface State {
  filter?: Filter;
  filteredData?: { results: { success: number, errors: number } };
}

const initialState: State = {
  filter: new Filter(),
  filteredData: { results: { success: 0, errors: 0 } }
};

export function reducer(state = initialState, action: settings.Actions): State {
  switch (action.type) {
    case settings.APPLY_FILTERS: {
      const filter = action.payload;
      console.log('inside apply filter reducer... ' + JSON.stringify(filter, null, 2));
      return updateObject<State>(state, {
        filter: filter,
        filteredData: { results: { success: 0, errors: 0 } }
      });
    }

    case settings.FILTER_SUCCESS: {
      const ret = action.payload;

      return updateObject<State>(state, {
        filter: ret.filter,
        filteredData: { results: ret.results }
      });
    }

    default:
      return state;
  }
}

export const getFilters = (state: State) => state.filter;
export const getFilteredData = (state: State) => state.filteredData;
