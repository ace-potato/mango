import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import * as settingsActions from './settings.action';

import { SettingsService } from './settings.service';

export type Action = settingsActions.Actions;
@Injectable()
export class SettingsEffects {
  constructor(private actions: Actions, private svc: SettingsService) { }

  // tslint:disable-next-line:member-ordering
  @Effect()
  getAccountData: Observable<Action> = this.actions
    .ofType(settingsActions.APPLY_FILTERS)
    .map((action: settingsActions.ApplyFiltersAction) => action.payload)
    .mergeMap(payload => this.svc.getAccountData(payload))
    .map(data => {
      return new settingsActions.ApplyFiltersSuccessAction(data);
    });
}
