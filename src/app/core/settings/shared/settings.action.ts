import { Action } from '@ngrx/store';
import { Filter, LogEntry, FilteredData } from './settings.model';
import { stringify } from '@angular/compiler/src/util';

export const APPLY_FILTERS = '[Filters] Apply Filters';
export const FILTER_SUCCESS = '[Filters] Apply Success';

export class ApplyFiltersAction implements Action {
  readonly type = APPLY_FILTERS;

  constructor(public payload: Filter) {
    console.log('inside ApplyFiltersAction');
  }
}
export class ApplyFiltersSuccessAction implements Action {
  readonly type = FILTER_SUCCESS;

  constructor(public payload: any) {}
}

export type Actions = ApplyFiltersAction | ApplyFiltersSuccessAction;
