import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './core/layout/layout.component';
import { authRoutes } from './pages/auth/auth.routing';
import { MapsModule } from 'app/pages/maps/maps.module';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',

        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: 'app/pages/scrumboard/scrumboard.module#FuseScrumboardModule',
        pathMatch: 'full'
      },
      {
        path: 'project',
        loadChildren: 'app/pages/scrumboard/project/project.module#ScrumboardProjectModule',
        pathMatch: 'prefix'
      },
      {
        path: 'map',
        loadChildren: 'app/pages/maps/maps.module#MapsModule',
        pathMatch: 'full'
      },
      {
        path: 'calendar',
        loadChildren: 'app/pages/calendar/calendar.module#CalendarModule',
        pathMatch: 'full'
      },
      {
        path: 'gantt',
        loadChildren: 'app/pages/gantt/gantt.module#GanttModule',
        pathMatch: 'full'
      },
      {
        path: 'inbox',
        loadChildren: 'app/pages/inbox/inbox.module#InboxModule',
        pathMatch: 'full'
      }

    ]
  },

  {
    path: 'auth',
    children: [
      ...authRoutes
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
